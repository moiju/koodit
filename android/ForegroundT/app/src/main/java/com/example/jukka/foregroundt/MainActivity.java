package com.example.jukka.foregroundt;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

    // Foreground Testi

public class MainActivity extends AppCompatActivity {
    public static final String CHANNEL_ID = "testikanava";
    public static int[] SELECTED_SENSORS = {1,1};
    private EditText editTextInput;

    TextView textView;
    int i = 0;
    fService fs = new fService();
    Config_Page config_page = new Config_Page();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextInput = findViewById(R.id.edit_text_input);
        textView = findViewById(R.id.text_view_eka);
        textView.setText("Config -> Start");

    }
    public void startService(View v){
        //config_page.loadData();
        String input = editTextInput.getText().toString();

        Intent serviceIntent = new Intent(this, fService.class);
        serviceIntent.putExtra("inputExtra", input);

        ContextCompat.startForegroundService(this, serviceIntent);
    }

   public void stopService(View v){
        Intent serviceIntent = new Intent(this, fService.class);
        stopService(serviceIntent);
   }

    public void config_page(View view) {
        Intent configIntent =  new Intent(this, Config_Page.class);
        startActivity(configIntent);
    }

    public void sensor_live_activity(View view) {
        Intent sensorIntent = new Intent(this, Sensors.class);
        startActivity(sensorIntent);
    }
} // loppu
