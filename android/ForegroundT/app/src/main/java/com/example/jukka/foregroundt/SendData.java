package com.example.jukka.foregroundt;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.example.jukka.foregroundt.MainActivity.SELECTED_SENSORS;

public class SendData extends AsyncTask <String, Void, String> {


    private String server_response;
    private Context mContext;
    //static String token;
    private int responseCode;
    private Context xContext;


    protected SendData(Context context){
       // mContext = context;
       // this.xContext = context.getApplicationContext();
    }

    @Override
    protected String doInBackground(String... strings) {
        String[] sensorNames = {"Light", "Proximity"};

        for (int i=0 ; i<SELECTED_SENSORS.length ; i++) {

            if (SELECTED_SENSORS[i] == 1) {
                String jsonData = "";
                jsonData = buildJson(strings[i], sensorNames[i]);

                sendData(jsonData);
            }

        }

        return null;

    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    @Override
    protected void onPostExecute(String s) {
        return;
    }
    private String buildJson(String value, String name){
        String jsonData = "";
        float f = Float.parseFloat(value);
        Long tsLong = System.currentTimeMillis();
        JSONObject postdata = new JSONObject();
        try {
            JSONArray jsonNimi = new JSONArray();
            jsonNimi.put(name);
            postdata.put("names", jsonNimi);
            JSONArray jsonArray1 = new JSONArray();
            JSONObject testi = new JSONObject();
            jsonArray1.put(f);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("timestamp", tsLong);
            jsonObject.put("values", jsonArray1);
            JSONArray jsonArray2 = new JSONArray();
            jsonArray2.put(jsonObject);
            postdata.put("valueRows", jsonArray2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonData = postdata.toString();

        return jsonData;
    }

    private void sendData(String jsonData){
        String sendUrl = "the actual send address here";

        URL url;
        BufferedReader reader = null;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL(sendUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
            writer.write(jsonData);
            writer.close();

            responseCode = urlConnection.getResponseCode();
            Log.e("RESPONSE",""+ responseCode);

            if(responseCode == HttpURLConnection.HTTP_OK){
                server_response = readStream(urlConnection.getInputStream());
                Log.i("CatalogClient", server_response);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e("IOEXCEPTION", "Error closing stream", e);
                }
            }
        }

    }
}
