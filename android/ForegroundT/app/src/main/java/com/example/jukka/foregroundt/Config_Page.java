package com.example.jukka.foregroundt;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Switch;

import static com.example.jukka.foregroundt.MainActivity.SELECTED_SENSORS;

public class Config_Page extends AppCompatActivity {

    private Switch sensorSwitch1;
    private Switch sensorSwitch2;
    public static final String SHARED_PREFERENCES = "Save";
    public static final String SWITCH1 = "Sensor1";
    public static final String SWITCH2 = "Sensor2";
    public static final String ASSET = "testi";

    private boolean sensor1;
    private boolean sensor2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_page);

        sensorSwitch1 = (Switch) findViewById(R.id.sensor_switch1);
        sensorSwitch2 = (Switch) findViewById(R.id.sensor_switch2);

        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveData();
    }

    public void sensor_setting1(View view) {

        if (sensorSwitch1.isChecked()){
            SELECTED_SENSORS[0] = 1;
        }
        else{
            SELECTED_SENSORS[0] = 0;
        }
        if(sensorSwitch2.isChecked()){
            SELECTED_SENSORS[1] = 1;
        }
        else{
            SELECTED_SENSORS[1] = 0;
        }
        saveData();

    }
    public void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(SWITCH1, sensorSwitch1.isChecked());
        editor.putBoolean(SWITCH2, sensorSwitch2.isChecked());

        editor.apply();


    }
    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        sensor1 = sharedPreferences.getBoolean(SWITCH1, true);
        sensor2 = sharedPreferences.getBoolean(SWITCH2, true);

        updatePage();

        if (sensorSwitch1.isChecked()){
            SELECTED_SENSORS[0] = 1;
        }
        else{
            SELECTED_SENSORS[0] = 0;
        }
        if(sensorSwitch2.isChecked()){
            SELECTED_SENSORS[1] = 1;
        }
        else{
            SELECTED_SENSORS[1] = 0;
        }
    }

    public void updatePage(){
        sensorSwitch1.setChecked(sensor1);
        sensorSwitch2.setChecked(sensor2);

    }
}
