package com.example.jukka.foregroundt;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.jukka.foregroundt.MainActivity.CHANNEL_ID;


public class fService extends Service {

    private Handler mHandler = new Handler();

    String[] apu;
    Sensors sensors = new Sensors();
    SensorService sensorService = new SensorService();
    SendData sendData = new SendData(this);

    int j = 1;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String input = intent.getStringExtra("inputExtra");

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,notificationIntent,0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("fService")
                .setContentText(input)
                .setSmallIcon(R.drawable.ic_aaa)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        mRunnable.run();

        return START_NOT_STICKY;
    }

    private Runnable mRunnable = new Runnable() {


        @Override
        public void run() {

            Intent serviceIntent = new Intent(getApplicationContext(), sensorService.getClass());

            if(!isMyServiceRunning(sensorService.getClass())){
                startService(serviceIntent);
            }

            mHandler.postDelayed(this,10000);
            //Toast.makeText(fService.this, "jtk "+j,Toast.LENGTH_SHORT).show();

            apu = sensorService.sensorM2();

            new SendData(getApplicationContext()).execute(apu);

            String input = ("valo: " + apu[0] );

            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,notificationIntent,0);

            Notification notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                    .setContentTitle("fService")
                    .setContentText(input)
                    .setSmallIcon(R.drawable.ic_aaa)
                    .setContentIntent(pendingIntent)
                    .build();

             startForeground(1, notification);
            j++;
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        Intent serviceIntent = new Intent(this, SensorService.class);
        stopService(serviceIntent);
        mHandler.removeCallbacksAndMessages(null);
    }

    private boolean isMyServiceRunning(Class<?> sClass){
        ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : activityManager.getRunningServices(Integer.MAX_VALUE)){
            if (sClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

}
