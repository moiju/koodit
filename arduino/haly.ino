#include "SIM900.h"
#include <SoftwareSerial.h>
#include "sms.h"
#include "call.h"
#include <LiquidCrystal.h>
#include <Keypad.h>
CallGSM call;
SMSGSM sms;

#define DEBUG

LiquidCrystal lcd(46, 47, 53, 52, 51, 50);

// nappaimisto
const byte ROWS = 4;
const byte COLS = 4;
char hexaKeys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[ROWS] = {29, 28, 27, 26}; 
byte colPins[COLS] = {25, 24, 23, 22}; 

Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS); 

// Hälyt
int aktiivinen_haly = 0;
int kaikki_halytykset = 0;
int kuittaus = 0;
int prio_kuittaus = 0;
int prio_haly = 0;
int kuittaus_soitto = 0;
String hviesti;
////
char number[20];
char admin_nro[20]="+358.. numero tähän";
char nappisPin[5] = "7352";
int nvirhe = 0;
int ntyhjays = 0;
byte stat=0;
byte stat2=0;
boolean started=false;
char pos;
char numba[20];
char viesti[100];
int i = 0;
int k = 0;
// Lämpö
int tempPin = 9;
float tempat = 0;
float aread = 0;
////
void luku();
void txt(String l_viesti);
void txt2(char c_viesti[20]);
void komento(char *oso, int tila);
void nappis(char nappi);
void rkytkenta(int ryhma, int r_tila);
Haly s1;
Haly s2;
// timerit
unsigned long loopCount = 0;
unsigned long timer_t = 0;
unsigned long timer_t2 = 0;
unsigned long timer_t3 = 0;
unsigned long timer_t4 = 0;


void setup() 
{
	s1.paalla = 1;
	s1.pin = 39;
	s1.tila = 0;
	s1.nimi = "IR 1 ";
	s1.prio = 2;

	s2.paalla = 1;
	s2.pin = 45;
	s2.tila = 0;
	s2.nimi = "ir eteinen ";
	s2.prio = 1;

	pinMode(s1.pin, INPUT);
	pinMode(s2.pin, INPUT);
	//Serial connection.
	Serial.begin(9600);
	
	lcd.begin(16, 2);
    lcd.print("start");
	
	Serial.println("GSM Shield testing.");
	if (gsm.begin(2400))
		Serial.println("\nstatus=READY");
	else Serial.println("\nstatus=IDLE");
	
	#ifdef DEBUG
	Serial.println("\nsoitto testi");
	//call.Call("numero tähän");
	Serial.println("\npirisee");
	#endif
	
};

void loop() 
{
	// tarkastetaan puhelut ja viestit halutuin väliajoin
	if ((millis() - timer_t2) > 3000) {
	//	Serial.println("puhelujen tarkastus");
		stat=call.CallStatusWithAuth(number,1,2);
		timer_t2 = millis();
	}
	if ((millis() - timer_t3) > 5000) {
	//	Serial.println("viestien tarkastus");
		pos = sms.IsSMSPresent(SMS_ALL);
		timer_t3 = millis();
	}
	
	// puhelun käsittely
	if(stat==CALL_INCOM_VOICE_AUTH || strcmp(number, admin_nro)==0 || stat==CALL_ACTIVE_VOICE){
		#ifdef DEBUG
        /*
		Serial.print("stat: ");
		Serial.println(stat);
		Serial.println("auth call");
		*/
		#endif
		if (kaikki_halytykset == 1 &&& aktiivinen_haly == 0){
			kaikki_halytykset = 0;
			delay(500);
			call.HangUp();
			#ifdef DEBUG
			Serial.println("Kytketaan halyt pois paalta");
			#endif
			}
			
		else if (kaikki_halytykset == 1 && aktiivinen_haly ==1){
			#ifdef DEBUG
			//Serial.println("vastataan puheluun");
			#endif
			
			delay(5000);
			call.PickUp();
			delay(15000);
			
			stat=call.CallStatusWithAuth(number,1,2);
			Serial.print("stat: ");
			Serial.println(stat);
					
			while(stat==CALL_ACTIVE_VOICE){
				#ifdef DEBUG
				Serial.println("\n"); 
				Serial.println("puhelu menossa");
				Serial.println("\n");
				#endif
				delay(15000);
				stat=call.CallStatusWithAuth(number,1,2);
				aktiivinen_haly = 0;
				kuittaus = 0;
				prio_kuittaus = 0;
				prio_haly = 0;
				lcd.setCursor(0, 1);
				lcd.print("       ");
			}
		}
		if(stat==CALL_INCOM_VOICE_NOT_AUTH){
			#ifdef DEBUG
			Serial.println("random soitto, lopetetaan");
			#endif
			call.HangUp();
		}
		if(stat==CALL_NONE){
			#ifdef DEBUG
			Serial.println("ei soittoa\n");
			#endif			
		}  
		number[0] = 0;
	}
	
	stat = 0;
	
	// viestin lukeminen
	if (pos>0){
		sms.GetSMS(pos, numba, viesti, 100);
		#ifdef DEBUG
		Serial.println("\n");
		Serial.print("numero");
		Serial.println(numba);
		Serial.print("viesti:");
		Serial.println(viesti);
		Serial.println("\n"); 
		#endif
		komento(viesti, strcmp(numba, admin_nro));
	  
			if(sms.DeleteSMS(pos)){
				#ifdef DEBUG
				Serial.println("\nmsg tuhottu\n");
				#endif
			 }
			else{
				#ifdef DEBUG 
				Serial.println("\nmsg tuhous failas\n");
				#endif
			}
		}
		pos = 0;
	if (kaikki_halytykset){
		luku();		
	}
	
	// nappaimiston lukeminen
	char nappi = customKeypad.getKey();
    if (nappi){
		#ifdef DEBUG 
		Serial.println("napin lukemiseen"); 
		#endif
		nappis(nappi);
    }
	if ((millis() - timer_t4) > 10000 && ntyhjays ==1) {
	lcd.setCursor(0, 1);
	lcd.print("                ");
	ntyhjays = 0;
	}
	
	// lampotila ja debug infoa
	if ((millis() - timer_t) > 20000) {
		
		aread = analogRead(tempPin);
        tempat = (5.0 * aread * 100)/1024;
		
		lcd.setCursor(0, 0);
		lcd.print("lampotila");
        lcd.setCursor(11, 0);
		lcd.print(tempat);
		
		#ifdef DEBUG
		Serial.print("lampotila: ");
		Serial.println(tempat);
        Serial.println("\n");
		Serial.print("Looppi pyöriny ");
		Serial.print(loopCount);
		Serial.println(" kertaa 20 sekunnin aikana");
		loopCount = 0;
		timer_t = millis();
		Serial.print("kaikk halyt paalla: ");
		Serial.println(kaikki_halytykset);
		Serial.print("aktiiviset halyt: ");
		Serial.println(aktiivinen_haly);
		Serial.println("\n");
        #endif		
	}
	loopCount++;
	};

void txt(String l_viesti){

    int str_len = l_viesti.length()+1;
	#ifdef DEBUG
    Serial.println("viestin lähetys");
    Serial.println("\n");
    Serial.println(l_viesti);
	#endif
	char viesti2[str_len];
    l_viesti.toCharArray(viesti2, str_len);
    /*
  if (sms.SendSMS("numero tähän", viesti2)){
		#ifdef DEBUG
		Serial.println("\nSMS sent OK");
		#endif
  }
  else{
		#ifdef DEBUG
		Serial.println("\nSMS send failed");
		#endif
  }
  */
}

void txt2(char c_viesti[20]){

	#ifdef DEBUG
    Serial.println("viestin lähetys");
    Serial.println("\n");
    Serial.println(c_viesti);
	#endif

	if (sms.SendSMS("numero tähän", c_viesti)){
		#ifdef DEBUG
		Serial.println("\nSMS sent OK");
		#endif
	}
	else{
		#ifdef DEBUG
		Serial.println("\nSMS send failed");
		#endif
	}
}
void komento(char *oso, int tila){

   // Kaikki hälyt päälle/pois
	char tempsend[20] = "";
	
	if ((strcmp(oso, "h on")==0) || (strcmp(oso, "H on")==0)){
		#ifdef DEBUG
		Serial.println("Kaikki halyt paalle\n");
		#endif
		kaikki_halytykset = 1;
	}
  
	if ((strcmp(oso, "h off")==0) || (strcmp(oso, "H off")==0)){
		#ifdef DEBUG
		Serial.println("Kaikki halyt pois paalta\n");
		#endif
		kaikki_halytykset = 0;
	}
  
  // Silmukka 1
	if ((strcmp(oso, "s1 on")==0) || (strcmp(oso, "S1 on")==0)){
		#ifdef DEBUG
		Serial.println("\nsilmukka 1 haly paalle\n"); 
		#endif
		s1.paalla=1;
	}
	if (strcmp(oso, "s1 off")==0||strcmp(oso, "S1 off")==0){
		#ifdef DEBUG
		Serial.println("\nsilmukka 1 halu pois paalta\n");
		#endif
		s1.paalla=0;
	}
	if ((strcmp(oso, "s1 tila")==0) || (strcmp(oso, "S1 tila")==0)){
		#ifdef DEBUG
		Serial.println("\tilakysely\n"); 
		#endif
		/*
		if (s1==1){
			tempsend ="s1 paalla";
			txt2(tempsend);
		}
		else if (s1==0){
			tempsend = "s1 pois paalta";
			txt2(tempsend);
		}
		*/
	}
	// Silmukka 2
	if ((strcmp(oso, "s2 on")==0) || (strcmp(oso, "S2 on")==0)){
		#ifdef DEBUG
		Serial.println("\nsilmukka 1 haly paalle\n"); 
		#endif
		s2.paalla=1;
	}
	if (strcmp(oso, "s2 off")==0||strcmp(oso, "S2 off")==0){
		#ifdef DEBUG
		Serial.println("\nsilmukka 1 halu pois paalta\n");
		#endif
		s2.paalla=0;
	}
	if ((strcmp(oso, "s2 tila")==0) || (strcmp(oso, "S2 tila")==0)){
		#ifdef DEBUG
		Serial.println("\ntilakysely\n"); 
		#endif
		/*
		if (s2==1){
			tempsend ="s2 paalla";
			txt2(tempsend);
		}
		else if (s2==0){
			tempsend = "s2 pois paalta";
			txt2(tempsend);
		}
		*/
	}
		if (strcmp(oso, "Lampo")==0||strcmp(oso, "lampo")==0){
		#ifdef DEBUG
		Serial.println("\nlampotila kysely\n");
		#endif
		dtostrf(tempat, 4, 2, tempsend);
		txt2(tempsend);
	}
	/*
	if (strcmp(oso, "r1 on")==0||strcmp(oso, "r1 no")==0){
		#ifdef DEBUG
		Serial.println("\nryhma 1 päälle\n");
		#endif
		rkytkenta(1, 1)
	}
	if (strcmp(oso, "R1 off")==0||strcmp(oso, "R1 off")==0){
		#ifdef DEBUG
		Serial.println("\nryhma 1 pois päältä\n");
		#endif
		rkytkenta(1, 0)
	}
	*/
}
void rkytkenta(int ryhma, int r_tila){
	
	// ryhmät 1 ja 2 ja niihin kuuluvat silmukat listattuna peräjälkeen.
	switch (ryhma) {
    case 1:
		s1.paalla = r_tila;
		s2.paalla = r_tila;
    break;
    case 2:
		/*
		s3.paalla = r_tila;
		s4.paalla = r_tila;
		*/
    break;
    default: 
		Serial.println("ryhmaa ei loydy");
    break;
  }
}

void luku(){
	
	// Silmukoiden lukeminen
	s1.tila = (digitalRead(s1.pin));
	if (s1.paalla == 1 && s1.tila == 1){
		#ifdef DEBUG
       // Serial.println("s1 halyttaa");
		#endif	
		aktiivinen_haly = 1;
		hviesti+= s1.nimi;
		if (s1.prio==2){
			prio_haly = 1;
		}
	}
	s2.tila = (digitalRead(s2.pin));
	if (s2.paalla == 1 && s2.tila == 1){
		#ifdef DEBUG
		Serial.println("s2 halyttaa");
		#endif
		aktiivinen_haly = 1;
		hviesti+= s2.nimi;
	}
	// Viestin lähetys
	if  (prio_haly == 1 && prio_kuittaus == 0){
		#ifdef DEBUG
		Serial.println("Halyjen kuittaus (prio)");
		#endif
		hviesti+= " halytystilassa";
		txt(hviesti);
		hviesti = "";

		kuittaus = 1;
		prio_kuittaus = 1;
		lcd.setCursor(k, 1);
		lcd.print("HALYTYS");
	}	
	else if (aktiivinen_haly == 1 && kuittaus == 0){
		#ifdef DEBUG
		Serial.println("halyjen kuittaus");
		#endif
		hviesti+= " halytystilassa";
		txt(hviesti);
		hviesti = "";
		kuittaus = 1;
		lcd.setCursor(k, 1);
		lcd.print("HALYTYS");
	}
}

void nappis(char nappi){
	Serial.println(nappi); 
	lcd.setCursor((11+k), 1);
	lcd.print("*");

	if (!(nappi == nappisPin[k])){
		nvirhe = 1;
		Serial.println("Virhe painallus"); 
		Serial.println(nappisPin[k]);
		Serial.println("painettu");
	}
	k++;
	if (k >= 4){
		k = 0;
		
		if(nvirhe == 0){
			if (kaikki_halytykset == 1){
				kaikki_halytykset = 0;
				aktiivinen_haly = 0;
				kuittaus = 0;
				prio_kuittaus = 0;
				prio_haly = 0;
				Serial.println("nappiksen koodi oikein, halyt pois ja kuittaus"); 
				lcd.setCursor(0, 1);
				lcd.print("oikea koodi");
			}
			else{
				kaikki_halytykset = 1;
				Serial.println("nappiksen koodi oikein, halyt paalle"); 
			}		  
		}
		
		else{
			k = 0; 
			
			lcd.setCursor(k, 1);
			lcd.print("Vaara koodi");
			Serial.println("väärä koodi"); 	
		}
		lcd.setCursor(11, 1);
		lcd.print("    ");
		nvirhe = 0;
		ntyhjays = 1;
	}		
	timer_t2 = millis();
    timer_t3 = millis();
	timer_t4 = millis();
}
