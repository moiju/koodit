import React, { useState, useEffect } from 'react';
import { Chart } from "react-google-charts";

const VastausChart = (props) => {
  // chartti päivitetään aina kun se avataan uudestaan

  function vastaustenTarkastus(tentit) {
    console.log("kaikki oikein checki")

    let tempData = [
      ["Element", "Virheita", { role: "style" }],
    ]

    const tentinKategoriat = ["default", "helpot", "keskinkertaiset", "vaikeat", "extra"]
    let oikeatVastauksetKategorioittain = [0, 0, 0, 0, 0]

    console.log(tentit[1].kysymykset[0].vastaukset[0])

    // Käydään tentit läpi ja lasketaan virheelliset/puuttuvat vastaukset kategorioittain

    tentit.forEach((tentti) => {
      tentti.kysymykset.forEach((kysymys) =>
        kysymys.vastaukset.forEach((vastaus) => {
          console.log("kysymys");
          console.log(kysymys)

          console.log("vastaus");
          console.log(vastaus)
          if (vastaus.oikeavastaus === true && vastaus.valittu === false) {

            console.log("oikea vastaus")
            console.log(kysymys.kategoria)
            oikeatVastauksetKategorioittain[kysymys.kategoria]++
          }
        }
        )
      )
    });

    // Päivitetään data taulu charttia varten

    oikeatVastauksetKategorioittain.forEach((vastaus, i) => {
      console.log("kategoriat:" + tentinKategoriat[i] + " " + vastaus);
      if (vastaus > 0) {
        tempData.push([tentinKategoriat[i], vastaus, "inherit"])
      }
    }
    )
    setData(tempData)
  }

  useEffect(() => {
    vastaustenTarkastus(props.tentit);
  }, [])

  const [data, setData] = useState([
    ["Element", "Virheita", { role: "style" }],
    ["testi", 3, "#b87333"],
    ["setti", 5, "green"]
  ]);

  return (

    < Chart
      chartType="ColumnChart"
      data={data}
      width="100%"
      height="400px"
    />
  )
}


export default VastausChart;
