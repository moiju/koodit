import React from 'react';
import { FormLabel, Checkbox, Box, TextField } from '@material-ui/core';

const Kysymys = (props) => {

  // Mapataan vastaukset ja vastausboxit kysymyksille, näytetään myös oikeat vastaukset jos ne on päällä

  return (
    <div>
      <TextField
        margin="normal"
        variant="filled"
        fullWidth
        multiline
        value={props.kysymys} />

      {props.vastaukset.map((vastaus, vastausIndex) => (
        <Box key={vastausIndex}>
          <FormLabel>
            <Checkbox color="primary" checked={vastaus.valittu || false} onChange={
              // (event) => props.action(event, props.tenttiIndex, props.kysymysIndex, i)
              (event) => props.dispatch(
                {
                  type: "valittuVastaus",
                  tenttiIndex: props.tenttiIndex,
                  kysymysIndex: props.kysymysIndex,
                  vastausIndex: vastausIndex
                })
            } >
            </Checkbox>{vastaus.vastaus}

            {props.naytaOikeatVastaukset &&
              <Checkbox color="secondary" checked={vastaus.oikeavastaus || false} ></Checkbox>
            }
          </FormLabel>
        </Box>))}
    </div>
  )
}

export default Kysymys;