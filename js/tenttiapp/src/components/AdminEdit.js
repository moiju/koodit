import React from 'react';
import { FormLabel, Checkbox, Box, TextField } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { makeStyles } from '@material-ui/core/styles';

const AdminEdit = (props) => {

  // Mapataan muokattavat kysymys ja vastaus kentät ja oikea/oikeat vastaukset checkboxi
  // Kysymysten ja vastausten lisäys/poisto

  const useStyles = makeStyles(theme => ({

    formControl: {
      margin: theme.spacing(2),
      minWidth: 120,
    },
  }));

  // ToDo -> kategoriat tarpeellisia? .. jos niin niiden muokkaaminen (lisää/poista/muuta)
  const [kysymyksenKatergoria, setKysymyksenKatergoria] = React.useState("");
  const classes = useStyles();

  return (
    <div >

      <TextField
        fullWidth
        style={{ margin: 10 }}
        variant="outlined"
        multiline

        value={props.kysymys || ""} onChange={
          // (event) => props.saveItems(event, props.tenttiIndex, props.kysymysIndex, 0, "kysymys")
          (event) => props.dispatch(
            {
              type: "kysymys",
              tenttiIndex: props.tenttiIndex,
              kysymysIndex: props.kysymysIndex,
              value: event.target.value
            })
        } />

      <FormControl className={classes.formControl}>
        <InputLabel id="label">Kategoria</InputLabel>
        <Select labelId="label" id="select"
          value={props.kategoria || ""}
          onChange={

            (event) => props.dispatch(
              {
                type: "kategoria",
                tenttiIndex: props.tenttiIndex,
                kysymysIndex: props.kysymysIndex,
                value: event.target.value
              })
          }
        >
          <MenuItem value={1}>1 helpot</MenuItem>
          <MenuItem value={2}>2 keskinkertaiset</MenuItem>
          <MenuItem value={3}>3 vaikeat</MenuItem>
          <MenuItem value={4}>4 extra</MenuItem>
        </Select>

      </FormControl>


      {
        props.vastaukset.map((vastaus, vastausIndex) => (
          <Box key={vastausIndex}>
            <FormLabel >
              <Checkbox color="primary" checked={vastaus.oikeavastaus || false} onChange={
                // (event) => props.saveItems(event, props.tenttiIndex, props.kysymysIndex, vastausIndex, "oikeaVastaus")
                (event) => props.dispatch(
                  {
                    type: "oikeaVastaus",
                    tenttiIndex: props.tenttiIndex,
                    kysymysIndex: props.kysymysIndex,
                    vastausIndex: vastausIndex,
                  })
              }></Checkbox>

              <TextField
                variant="filled"
                style={{ margin: 8 }}
                multiline
                value={vastaus.vastaus || ""} onChange={
                  // (event) => props.saveItems(event, props.tenttiIndex, props.kysymysIndex, vastausIndex, "vastaus")
                  (event) => props.dispatch(
                    {
                      type: "vastaus",
                      tenttiIndex: props.tenttiIndex,
                      kysymysIndex: props.kysymysIndex,
                      vastausIndex: vastausIndex,
                      value: event.target.value
                    })
                } />

              <Button
                variant="contained"
                color="secondary"
                size="small"
                startIcon={<DeleteIcon />}
                onClick={
                  // (event) => props.saveItems(event, props.tenttiIndex, props.kysymysIndex, vastausIndex, "poistaVastaus")
                  (event) => props.dispatch(
                    {
                      type: "poistaVastaus",
                      tenttiIndex: props.tenttiIndex,
                      kysymysIndex: props.kysymysIndex,
                      vastausIndex: vastausIndex
                    })
                }></Button>
            </FormLabel>
          </Box>))
      }
      <div>
        <FormLabel>
          <Button variant="contained" color="primary" size="small" onClick={
            // (event) => props.saveItems(event, props.tenttiIndex, props.kysymysIndex, 0, "uusiVastaus")
            (event) => props.dispatch(
              {
                type: "uusiVastaus",
                tenttiIndex: props.tenttiIndex,
                kysymysIndex: props.kysymysIndex,
                event: event.target.checked
              })
          }>Lisaa vastaus</Button>
          <Button variant="contained" color="secondary" size="small" onClick={
            // (event) => props.saveItems(event, props.tenttiIndex, props.kysymysIndex, 0, "poistaKysymys")
            (event) => props.dispatch(
              {
                type: "poistaKysymys",
                tenttiIndex: props.tenttiIndex,
                kysymysIndex: props.kysymysIndex,
                event: event.target.checked
              })
          }>Poista kysymys</Button>
        </FormLabel>
      </div>
    </div>
  )
}

export default AdminEdit;