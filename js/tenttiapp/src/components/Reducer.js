import React from 'react';

export default function reducer(state, action) {

  let nykyinenData = JSON.parse(JSON.stringify(state))

  switch (action.type) {

    case "kysymys":
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].kysymys = action.value

      break;

    case "nimi":
      nykyinenData[action.tenttiIndex].nimi = action.value

      break;

    case "kategoria":
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].kategoria = action.value
      console.log("kategoria")
      console.log(nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].kategoria)

      break;

    case "vastaus":
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset[action.vastausIndex].vastaus = action.value
      console.log("vastaus tallennus")
      console.log(action.value)

      break;

    case "uusiVastaus":
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset.push({ vastaus: "", valittu: false, oikeavastaus: false })

      break;

    case "poistaVastaus":
      console.log("vastausindexi: " + action.vastausIndex)
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset.splice(action.vastausIndex, 1)

      break;

    case "oikeaVastaus":
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset[action.vastausIndex].oikeavastaus = (!nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset[action.vastausIndex].oikeavastaus)

      break;

    case "valittuVastaus":
      nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset[action.vastausIndex].valittu = (!nykyinenData[action.tenttiIndex].kysymykset[action.kysymysIndex].vastaukset[action.vastausIndex].valittu)

      break;

    case "lisaaKysymys":
      nykyinenData[action.tenttiIndex].kysymykset.push(
        {
          kysymys: "default", kategoria: 1, vastaukset: [{ vastaus: "joo", valittu: false, oikeavastaus: false },
          { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: false }]
        }
      )

      break;

    case "poistaKysymys":
      console.log("kysymysindexi: " + action.kysymysIndex)
      nykyinenData[action.tenttiIndex].kysymykset.splice(action.kysymysIndex, 1)

      break;

    case "lisaaTentti":
      nykyinenData.push(
        {
          nimi: "uusi tentti", kysymykset: [{
            kysymys: "default", kategoria: 1, vastaukset: [{ vastaus: "joo", valittu: false, oikeavastaus: false },
            { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: false }]
          },
          {
            kysymys: "default", kategoria: 1, vastaukset: [{ vastaus: "joo", valittu: false },
            { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: false }]
          }]
        }
      )
      break;

    case "poistaTentti":
      nykyinenData.splice(action.tenttiIndex, 1)
      // setValittuTentti(null)

      break;

    default:
      throw new Error();
  }
  return nykyinenData;
}
