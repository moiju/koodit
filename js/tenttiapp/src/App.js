import React, { useState, useEffect, Fragment, Input, useReducer } from 'react';
import './App.css';

import { FormLabel, Checkbox, Box, TextField } from '@material-ui/core';
import { makeStyles, createPalette } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import { render } from "react-dom";
import { Chart } from "react-google-charts";
import { switchCase } from '@babel/types';

//import { default as UUID } from "node-uuid";

import reducer from "./components/Reducer"
import AdminEdit from "./components/AdminEdit"
import Kysymys from "./components/Kysymys"
import VastausChart from "./components/VastausChart"

import { green, red } from '@material-ui/core/colors';

const useStyles = makeStyles(theme => ({

  root: {
    flexGrow: 1,
  },
  sivu: {
    margin: 10,
  }
}));

// Vakio tentti mikä jää voimaan, jos ei localdatasta löydy tenttiä. Myöhemmin tenttien lataus backendiin.

const initialState = [{
  nimi: "Tentti 1", kysymykset: [{
    kysymys: "Kysymys 1", kategoria: 1,
    vastaukset: [{ vastaus: "kyllä", valittu: false, oikeavastaus: true }, { vastaus: "ehkä", valittu: false, oikeavastaus: false },
    { vastaus: "ei", valittu: false, oikeavastaus: true }]
  },
  {
    kysymys: "Kysymys 2", kategoria: 2, vastaukset: [{ vastaus: "y", valittu: false, oikeavastaus: false },
    { vastaus: "n/a", valittu: false, oikeavastaus: false },
    { vastaus: "n", valittu: false, oikeavastaus: true }]
  },
  {
    kysymys: "Kysymys 3", kategoria: 2, vastaukset: [{ vastaus: "y", valittu: false, oikeavastaus: false },
    { vastaus: "n/a", valittu: false, oikeavastaus: false },
    { vastaus: "n", valittu: false, oikeavastaus: true }]
  }]
},
{
  nimi: "Tentti 2", kysymykset: [{
    kysymys: "Kysymys 4", kategoria: 3, vastaukset: [{ vastaus: "joo", valittu: false, oikeavastaus: false },
    { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: true }]
  },
  {
    kysymys: "Kysymys 5", kategoria: 2, vastaukset: [{ vastaus: "joo", valittu: false },
    { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: true }]
  }]
},
{
  nimi: "Tentti 3", kysymykset: [{
    kysymys: "Kysymys 6", kategoria: 1, vastaukset: [{ vastaus: "joo", valittu: false, oikeavastaus: false },
    { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: true }]
  },
  {
    kysymys: "Kysymys 7", kategoria: 2, vastaukset: [{ vastaus: "joo", valittu: false },
    { vastaus: "jaa", valittu: false, oikeavastaus: false }, { vastaus: "ei", valittu: false, oikeavastaus: true }]
  }]
}
];


const App = () => {

  function Poistalocaldata() {
    localStorage.clear();
    console.log("datan poisto")
  }

  const [tentit, dispatch] = useReducer(reducer, initialState);
  const [valittuTentti, setValittuTentti] = useState(null)

  // Ladataan localstoragesta tentti
  // ToDo -> tilapäinen ratkaisu.. tallennus backendiin?
  useEffect(() => {

    let t = localStorage.getItem("tenttiappis")
    // if (t) setTentit(JSON.parse(t));
  }, [])

  // Päivitetään tenttiä localstoragessa aina kuin se muuttuu

  useEffect(() => {

    localStorage.setItem("tenttiappis", JSON.stringify(tentit))

  }, [tentit])


  const [naytaOikeatVastaukset, setnaytaOikeatVastaukset] = useState(false)
  function Oikeatvastaukset() {

    console.log("")

    if (naytaOikeatVastaukset === false) {
      setnaytaOikeatVastaukset(true)
      console.log("vastauten vaihto paalle")
    } else {
      setnaytaOikeatVastaukset(false)
      console.log("vastauten vaihto pois")
    }
    console.log("")
  }

  const [naytaKysymykset, setnaytaKysymykset] = useState(false)
  const [adminUser, setAdminuser] = useState(false)
  const [naytaChart, setNaytaChart] = useState(false)
  const classes = useStyles();


  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>

          <Button color="inherit" onClick={Poistalocaldata}>Poista localdata</Button>
          <Button color="inherit" onClick={Oikeatvastaukset}>oikeat vastaukset</Button>
          <Button color="inherit" onClick={() => setnaytaKysymykset(!naytaKysymykset)}>kysymykset</Button>
          <Button color="inherit" onClick={() => { setValittuTentti(null); setNaytaChart(!naytaChart) }}>Chartti</Button>
          <Button color="inherit" onClick={() => setAdminuser(!adminUser)}>ADMIN</Button>
        </Toolbar>
      </AppBar>

      <div className={classes.sivu}>

        {naytaKysymykset && !adminUser &&
          // mapataan tentit buttoneiksi
          <div>
            {
              tentit.map((tentti, i) => <Button variant={valittuTentti === i ? "outlined" : "contained"} color="primary"
                onClick={() => setValittuTentti(i)}>{tentti.nimi}</Button>
              )}
          </div>
        }

        {naytaKysymykset && !adminUser &&
          valittuTentti !== null && tentit[valittuTentti].kysymykset.map((kysymys, kysymysIndex) =>
            <Kysymys kysymysIndex={kysymysIndex}  {...kysymys} tenttiIndex={valittuTentti} dispatch={dispatch}
              naytaOikeatVastaukset={naytaOikeatVastaukset} />)
        }

        {adminUser &&
          // mapataan tentit buttoneiksi admin modessa
          <div>
            {
              tentit.map((tentti, i) => <Button variant={valittuTentti === i ? "outlined" : "contained"} color="secondary"
                onClick={() => setValittuTentti(i)}>{tentti.nimi}</Button>
              )}
          </div>
        }

        {adminUser && !naytaChart &&
          <Button variant="contained" color="primary" style={{ margin: 10 }} size="large" onClick={
            // (event) => handleSaveItems(event, valittuTentti, 0, 0, "lisaaTentti")
            (event) => dispatch(
              {
                type: "lisaaTentti",
                tenttiIndex: valittuTentti,
                event: event.target.checked
              })
          } >Lisää Tentti</Button>
        }

        {adminUser && valittuTentti !== null &&
          <Button variant="contained" color="secondary" style={{ margin: 10 }} size="large" onClick={
            // (event) => handleSaveItems(event, valittuTentti, 0, 0, "a")
            (event) => {
              dispatch(
                {
                  type: "poistaTentti",
                  tenttiIndex: valittuTentti,
                  event: event.target.checked
                });
              setValittuTentti(null)
            }
          } >Poista Tentti</Button>
        }

        {adminUser &&
          valittuTentti !== null &&
          <TextField
            style={{ margin: 10 }}
            variant="outlined"
            label="tentin nimi"
            value={tentit[valittuTentti].nimi || ""} onChange={
              (event) => dispatch(
                {
                  type: "nimi",
                  tenttiIndex: valittuTentti,
                  value: event.target.value
                })
            } />
        }

        {adminUser &&
          valittuTentti !== null && tentit[valittuTentti].kysymykset.map((kysymys, kysymysIndex) =>
            <AdminEdit kysymysIndex={kysymysIndex} {...kysymys} nimi={tentit[valittuTentti].nimi} tenttiIndex={valittuTentti} dispatch={dispatch}
              naytaOikeatVastaukset={naytaOikeatVastaukset} kategoria={kysymys.kategoria} />)
        }

        <br></br>
        <br></br>

        {adminUser && valittuTentti !== null &&
          <Button variant="contained" style={{ margin: 10, color: 'green' }} size="large" onClick={
            // (event) => handleSaveItems(event, valittuTentti, 0, 0, "lisaaKysymys")
            (event) => dispatch(
              {
                type: "lisaaKysymys",
                tenttiIndex: valittuTentti,
                event: event.target.checked
              })
          } >Lisää kysymys</Button>
        }

        {naytaChart &&
          <VastausChart tentit={tentit} />
        }
      </div>

    </div>
  );
}

export default App;